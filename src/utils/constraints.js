export const RoundStatus = {
  INVALID: 0,
  PENDING: 'pending',
  RUNNING: 'running',
  FINISHED: 'finished',
}

export const Standard = {
  FULL: 'full',
  SEASONAL: 'seasonal',
  OTHER: 'other',
}

export const StandardMap = new Map([
  [Standard.FULL, {
    name: "Full",
  }],
  [Standard.SEASONAL, {
    name: "Seasonal",
  }],
  [Standard.OTHER, {
    name: "Other",
  }],
])

export const Choice = {
  NORMAL: 'normal',
  THREE_ONE: 'three-one',
  SIMPLE_MYTHICAL: 'simple-mythical',
  EACH_GAME: 'each-game',
  TEAM: 'team',
  MYTHICAL: 'mythical',
  OTHER: 'other',
}

export const ChoiceMap = new Map([
  [Choice.NORMAL, {
    name: "Normal",
  }],
  [Choice.THREE_ONE, {
    name: "Three - One",
  }],
  [Choice.SIMPLE_MYTHICAL, {
    name: "Simple Mythical",
  }],
  [Choice.EACH_GAME, {
    name: "Each Game",
  }],
  [Choice.TEAM, {
    name: "Team",
  }],
  [Choice.MYTHICAL, {
    name: "Mythical",
  }],
  [Choice.OTHER, {
    name: "Other",
  }],
])

export const SortDirection = {
  ASC: 'ascend',
  DESC: 'descend'
}
