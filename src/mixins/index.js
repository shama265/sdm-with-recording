const iconAttrData = {
  data() {
    return {
      iconAttr: {
        success: {
          icon: "check-circle",
          class: "text-success",
        },
        warning: {
          icon: "exclamation-triangle",
          class: "text-warning",
        },
        danger: {
          icon: "times-circle",
          class: "text-danger",
        },      
      }
    }
  }
}

const invalidFormMessageData = {
  data() {
    return {
      invalidFormMessage: {
        required: "Field is required",
        integer: "Must be integer",
        decimal: "Must be decimal",
        minValue: num => `Must be ${num} or higher`,
      },
    }
  }
}

export { iconAttrData, invalidFormMessageData }
