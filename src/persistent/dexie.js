import Dexie from 'dexie'

const db = new Dexie('sdmDb')
db.version(1).stores({
  events: `++id, startTime`,
})

export default db
