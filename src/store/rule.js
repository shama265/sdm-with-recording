import localStorage from '@/persistent/localstorage'
import { Standard, Choice } from '@/utils/constraints'
import { SortDirection } from '@/utils/constraints'

const state = {
  eventName: "Sakura Brawl",
  organizer: "Goo-J1",
  pointWin: 3,
  pointDraw: 1,
  tableSize: 2,
  minPlayer: 4,
  maxPlayer: 8,
  maxRound: 3,
  standard: Standard.FULL,
  choice: Choice.NORMAL,
  tieBreaksEnabled: [
    {
      key: 'point',
      name: 'Point',
      priorityFixed: true,
      direction: SortDirection.DESC,
    },
    {
      key: 'solkoff',
      name: 'Solkoff',
      direction: SortDirection.DESC,
    },
  ],
  tieBreaksDisabled: [
    {
      key: 'oppMwRate',
      name: 'OppMW%',
      direction: SortDirection.DESC,
      format: 'rate',
    },
  ],
}

try {
  const defaultRule = JSON.parse(localStorage.getItem('defaultRules'))
  if(defaultRule) {
    Object.assign(state, defaultRule)
    console.debug('default rules are loaded')
  }
} catch {
  console.warn('failure to get data from localStorage')
}

export default {
  state,
  getters: {
  },
  mutations: {
    setRule(state, payload) {
      state.eventName = payload.eventName
      state.organizer = payload.organizer
      state.maxPlayer = payload.maxPlayer
      state.maxRound = payload.maxRound
      state.pointWin = payload.pointWin
      state.pointDraw = payload.pointDraw
      state.standard = payload.standard
      state.choice = payload.choice
      state.tieBreaksEnabled = payload.tieBreaksEnabled
      state.tieBreaksDisabled = payload.tieBreaksDisabled
    },
    restoreData(state, { rule }) {
      state.eventName = rule.eventName
      state.organizer = rule.organizer
      state.maxPlayer = rule.maxPlayer
      state.maxRound = rule.maxRound
      state.pointWin = rule.pointWin
      state.pointDraw = rule.pointDraw
      state.standard = rule.standard
      state.choice = rule.choice
      state.tieBreaksEnabled = rule.tieBreaksEnabled
      state.tieBreaksDisabled = rule.tieBreaksDisabled
    },
  }
}