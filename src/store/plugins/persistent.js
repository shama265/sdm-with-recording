const hooks = {
  async startEvent({db, state}) {
    await db.events.put({
      startTime: state.startTime,
      sentResult: state.sentResult,
      player: state.player,
      table: state.table,
      round: state.round,
      rule: state.rule,
    })
  },
  clearTables: updateTable,
  addTables: updateTable,
  setTableResult: updateTable,
  addPlay: updatePlayer,
  dropPlayer: updatePlayer,
  advanceRound: updateRound,
  startRound: updateRound,
  finishRound: updateRound,
  doneSendResult: updateSentResult,
}

async function updateTable({db, state}) {
  await db.events
  .where('startTime')
  .equals(state.startTime)
  .modify({
    table: state.table
  })
}

async function updatePlayer({db, state}) {
  await db.events
  .where('startTime')
  .equals(state.startTime)
  .modify({
    player: state.player
  })
}

async function updateRound({db, state}) {
  await db.events
  .where('startTime')
  .equals(state.startTime)
  .modify({
    round: state.round
  })
}

async function updateSentResult({db, state}) {
  await db.events
  .where('startTime')
  .equals(state.startTime)
  .modify({
    sentResult: state.sentResult
  })
}

export default function(db) {
  return store => {
    store.subscribe((mutation, state) => {
      hooks[mutation.type] && hooks[mutation.type]({db, state})
      .then(() => {
        console.debug('auto save success.')
      })
      .catch(e => {
        console.error('persistent error: ' + e.stack)
      })
    })
  }
}
