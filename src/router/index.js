import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '@/views/Home.vue'
import Rule from '@/views/Rule.vue'
import CheckIn from '@/views/CheckIn.vue'
import Leaderboard from '@/views/Leaderboard.vue'
import Tables from '@/views/Tables.vue'
import Config from '@/views/Config.vue'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'Home',
    component: Home
  },
  {
    path: '/rule',
    name: 'Rule',
    component: Rule
  },
  {
    path: '/check-in',
    name: 'CheckIn',
    component: CheckIn
  },
  {
    path: '/leaderboard',
    name: 'Leadeboard',
    component: Leaderboard
  },
  {
    path: '/tables',
    name: 'Tables',
    component: Tables
  },
  {
    path: '/config',
    name: 'Config',
    component: Config
  },
]

const router = new VueRouter({
  routes
})

export default router
