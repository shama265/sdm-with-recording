process.env.VUE_APP_VERSION = require('./package.json').version

module.exports = {
  publicPath: process.env.NODE_ENV === 'production' ? './' : '/',
  pwa: {
    name: process.env.VUE_APP_TITLE,
    appleMobileWebAppCapable: 'yes',
    themeColor: "#9C009C",
    iconPaths: {
      favicon32: 'img/icons/favicon-32x32.png',
      favicon16: 'img/icons/favicon-16x16.png',
      appleTouchIcon: 'img/icons/apple-touch-icon.png',
      maskIcon: 'img/icons/safari-pinned-tab.svg',
      msTileImage: 'img/icons/mstile-144x144.png'
    },
    workboxOptions: {
      skipWaiting: true,
    }
  },
  chainWebpack: config => {
    config
    .plugin('html')
    .tap(args => {
      args[0].title = process.env.VUE_APP_TITLE
      return args
    })
  },
}
