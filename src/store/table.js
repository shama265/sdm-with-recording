export default {
  state: {
    list: [],
  },
  getters: {
  },
  mutations: {
    addTables(state, payload) {
      state.list.push(...payload.tables)
    },
    clearTables(state) {
      state.list = []
    },
    setTableResult(state, payload) {
      let table = state.list.find(e => e.seq === payload.tableSeq)
      table.result = {
        winner: payload.winner,
        isDraw: payload.isDraw,
      }
    },
    restoreData(state, { table }) {
      state.list = table.list
    },
  },
  actions: {
    saveTableResult({ commit }, result) {
      commit('setTableResult', result)
    }
  },
}